import { Directive, HostListener, HostBinding } from '@angular/core';

@Directive({
  selector: '[appContainerNavbar]'
})
export class ContainerNavbarDirective {

  constructor() { }

  @HostBinding('ngClass') valorNavbar = true;

  @HostListener('click') aoClicar() {
    this.valorNavbar = !this.valorNavbar;
    console.log(this.valorNavbar);
  }

}
