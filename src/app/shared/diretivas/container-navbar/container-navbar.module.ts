import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContainerNavbarDirective } from './container-navbar.directive';

@NgModule({
  declarations: [ContainerNavbarDirective],
  imports: [
    CommonModule
  ],
  exports: [ContainerNavbarDirective]
})
export class ContainerNavbarModule { }
