import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from './material/material.module';
import { LayoutModule } from '@angular/cdk/layout';
import { PrimengModule } from './primeng/primeng.module';
import { ContainerNavbarModule } from './diretivas/container-navbar/container-navbar.module';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MaterialModule,
    PrimengModule,
    LayoutModule,
    ContainerNavbarModule,
    RouterModule
  ],
  exports: [
    MaterialModule,
    PrimengModule,
    LayoutModule,
    ContainerNavbarModule,
    RouterModule
  ]
})
export class SharedModule { }
