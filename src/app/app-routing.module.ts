import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PesquisaProdutoComponent } from './produto/pesquisa-produto/pesquisa-produto.component';
import { PesquisaClienteComponent } from './cliente/pesquisa-cliente/pesquisa-cliente.component';
import { CadastraProdutoComponent } from './produto/cadastra-produto/cadastra-produto.component';
import { CadastraClienteComponent } from './cliente/cadastra-cliente/cadastra-cliente.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PesquisaVendedorComponent } from './vendedor/pesquisa-vendedor/pesquisa-vendedor.component';
import { CadastraVendedorComponent } from './vendedor/cadastra-vendedor/cadastra-vendedor.component';

const routes: Routes = [
  {
    path: 'dashboard',
    component: DashboardComponent
  },
  {
    path: 'produto',
    component: PesquisaProdutoComponent
  },
  {
    path: 'cadastraProduto',
    component: CadastraProdutoComponent
  },
  {
    path: 'cliente',
    component: PesquisaClienteComponent
  },
  {
    path: 'cadastraCliente',
    component: CadastraClienteComponent
  },
  {
    path: 'vendedor',
    component: PesquisaVendedorComponent
  },
  {
    path: 'cadastraVendedor',
    component: CadastraVendedorComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
