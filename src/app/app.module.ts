import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { registerLocaleData } from '@angular/common';
import localePt from '@angular/common/locales/pt';

import { AppComponent } from './app.component';
import { ProdutoService } from './produto/produto.service';
import { ClienteService } from './cliente/cliente.service';
import { ProdutoModule } from './produto/produto.module';
import { ClienteModule } from './cliente/cliente.module';
import { SharedModule } from './shared/shared.module';
import { NavbarModule } from './core/navbar/navbar.module';
import { CoreModule } from './core/core.module';

import { CurrencyMaskConfig, CURRENCY_MASK_CONFIG } from 'ng2-currency-mask/src/currency-mask.config';
import { LoginFormComponent } from './seguranca/login-form/login-form.component';
import { AppRoutingModule } from './app-routing.module';
import { DashboardModule } from './dashboard/dashboard.module';
import { VendedorModule } from './vendedor/vendedor.module';
import { VendedorService } from './vendedor/vendedor.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

export const CustomCurrencyMaskConfig: CurrencyMaskConfig = {
  align: 'right',
  allowNegative: true,
  decimal: ',',
  precision: 2,
  prefix: 'R$ ',
  suffix: '',
  thousands: '.'
};

registerLocaleData(localePt);

@NgModule({
  declarations: [
    AppComponent,
    LoginFormComponent
  ],
  imports: [
    BrowserAnimationsModule,
    SharedModule,
    CoreModule,
    BrowserModule,
    ProdutoModule,
    ClienteModule,
    VendedorModule,
    HttpClientModule,
    NavbarModule,
    ReactiveFormsModule,
    FormsModule,
    AppRoutingModule,
    DashboardModule
  ],
  exports: [],
  providers: [
    ProdutoService,
    ClienteService,
    VendedorService,
    { provide: LOCALE_ID, useValue: 'pt-BR' },
    { provide: CURRENCY_MASK_CONFIG, useValue: CustomCurrencyMaskConfig }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
