import { Component, OnInit } from '@angular/core';
import { VendedorFilter } from 'src/app/core/model/VendedorFilter';
import { VendedorService } from '../vendedor.service';
import { NavbarService } from 'src/app/core/navbar/navbar.service';
import { Router } from '@angular/router';
import { ToastyService } from 'ng2-toasty';

@Component({
  selector: 'app-pesquisa-vendedor',
  templateUrl: './pesquisa-vendedor.component.html',
  styleUrls: ['./pesquisa-vendedor.component.css']
})
export class PesquisaVendedorComponent implements OnInit {

  filtro = new VendedorFilter();
  vendedores = [];
  totalRegistros = 0;
  nav = false;

  constructor(
    private vendedorService: VendedorService,
    private navbarService: NavbarService,
    private router: Router,
    private toasty: ToastyService
  ) { }

  ngOnInit() {
    this.navbarService.eventContainer.subscribe((event: boolean) => this.setNav(event));
    this.pesquisar();
  }

  pesquisar(pagina = 0) {

    this.filtro.pagina = pagina;

    this.vendedorService.listarVendedores(this.filtro)
      .subscribe(response => {
        this.vendedores = response.content;
        this.totalRegistros = response.totalElements;
      }, err => {
        this.toasty.error('Erro ao listar vendedores');
      });
  }

  setNav(visibilidade: boolean) {
    this.nav = visibilidade;
  }

  novoVendedor() {
    this.router.navigate([`/cadastraVendedor`]);
  }

}
