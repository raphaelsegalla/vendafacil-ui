import { Component, OnInit, Input } from '@angular/core';
import { VendedorFilter } from 'src/app/core/model/VendedorFilter';
import { PesquisaVendedorComponent } from '../pesquisa-vendedor/pesquisa-vendedor.component';
import { VendedorService } from '../vendedor.service';
import { LazyLoadEvent, ConfirmationService } from 'primeng/primeng';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { ToastyService } from 'ng2-toasty';

@Component({
  selector: 'app-lista-vendedor',
  templateUrl: './lista-vendedor.component.html',
  styleUrls: ['./lista-vendedor.component.css'],
  animations: [
    trigger('rowExpansionTrigger', [
        state('void', style({
            transform: 'translateX(-10%)',
            opacity: 0
        })),
        state('active', style({
            transform: 'translateX(0)',
            opacity: 1
        })),
        transition('* <=> *', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)'))
    ])
 ]
})
export class ListaVendedorComponent implements OnInit {

  @Input() vendedores = [];
  @Input() totalRegistros: number;
  @Input() filtro = new VendedorFilter();

  pagina: number;

  constructor(
    private pesquisaVendedor: PesquisaVendedorComponent,
    private vendedorService: VendedorService,
    private toasty: ToastyService,
    private confirmationService: ConfirmationService
  ) { }

  ngOnInit() {
  }

  aoMudarPagina(event: LazyLoadEvent ) {
    const pagina = event.first / event.rows;
    this.pagina = pagina;
    this.pesquisaVendedor.pesquisar(pagina);
  }

  excluir(vendedor: any) {
    this.vendedorService.excluir(vendedor.codigo)
    .subscribe( () => {
      this.pesquisaVendedor.pesquisar(this.pagina);
      this.toasty.success(`Vendedor "${vendedor.nome}" excluido com sucesso`);
    }, err => {
      this.toasty.error('Erro ao excluir vendedor');
    });
  }

  confirmarExclusao(vendedor: any) {
    this.confirmationService.confirm({
      message: `Tem certeza que deseja excluir o(a) vendedor "${vendedor.nome}"`,
      accept: () => {
        this.excluir(vendedor);
      }
    });
  }

}
