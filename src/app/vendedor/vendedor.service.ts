import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { VendedorFilter } from '../core/model/VendedorFilter';
import { Observable } from 'rxjs';
import { VendedorForm } from '../core/model/VendedorForm';

@Injectable({
  providedIn: 'root'
})
export class VendedorService {

  vendedorUrl = 'http://localhost:8080/vendedores';

  constructor(private http: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      Authorization: 'Basic YWRtaW5AZ21haWwuY29tOmFkbWlu'
    })
  };

  listarVendedores(filtro: VendedorFilter): Observable<any> {

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: 'Basic YWRtaW5AZ21haWwuY29tOmFkbWlu'
      }),
      params: new HttpParams()
      .set('nome', filtro.nome === undefined ? '' : filtro.nome)
      .set('email', filtro.email === undefined ? '' : filtro.email)
      .set('telefone', filtro.telefone === undefined ? '' : filtro.telefone)
      .set('logradouro', filtro.logradouro === undefined ? '' : filtro.logradouro)
      .set('page', filtro.pagina.toString())
      .set('size', filtro.itensPorPagina.toString())

    };
    return this.http.get(`${this.vendedorUrl}`, httpOptions );
  }

  criar(vendedor: VendedorForm): Observable<VendedorForm> {
    vendedor.ativo = true;
    return this.http.post<VendedorForm>(`${this.vendedorUrl}`, vendedor, this.httpOptions );
  }

  excluir(codigo: number) {
    return this.http.delete(`${this.vendedorUrl}/${codigo}`, this.httpOptions);
  }
}
