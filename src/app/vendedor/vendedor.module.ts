import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PesquisaVendedorComponent } from './pesquisa-vendedor/pesquisa-vendedor.component';
import { CadastraVendedorComponent } from './cadastra-vendedor/cadastra-vendedor.component';
import { ListaVendedorComponent } from './lista-vendedor/lista-vendedor.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from '../shared/shared.module';
import { CurrencyMaskModule } from 'ng2-currency-mask';

@NgModule({
  declarations: [
    PesquisaVendedorComponent,
    CadastraVendedorComponent,
    ListaVendedorComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    SharedModule,
    CurrencyMaskModule,
  ],
  exports: [
    PesquisaVendedorComponent,
    CadastraVendedorComponent
  ]
})
export class VendedorModule { }
