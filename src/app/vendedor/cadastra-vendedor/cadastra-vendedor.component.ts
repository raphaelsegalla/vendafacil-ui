import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { VendedorService } from '../vendedor.service';
import { ConsultaCepService } from 'src/app/shared/services/consulta-cep.service';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { NavbarService } from 'src/app/core/navbar/navbar.service';
import { ToastyService } from 'ng2-toasty';

@Component({
  selector: 'app-cadastra-vendedor',
  templateUrl: './cadastra-vendedor.component.html',
  styleUrls: ['./cadastra-vendedor.component.css']
})
export class CadastraVendedorComponent implements OnInit {

  subscription: Subscription;
  vendedorForm: FormGroup;
  nav = false;

  constructor(
    private vendedorService: VendedorService,
    private cepService: ConsultaCepService,
    private formBuilder: FormBuilder,
    private router: Router,
    private navbarService: NavbarService,
    private toasty: ToastyService
  ) { }

  ngOnInit() {
    this.navbarService.eventContainer.subscribe(
      event => this.setNav(event)
    );

    this.vendedorForm = this.formBuilder.group({
      nome: [null, [Validators.required, Validators.minLength(5), Validators.maxLength(50)]],
      email: [null, [Validators.required, Validators.email]],
      cpf: [null, [Validators.required]],
      telefone: [null, [Validators.required]],
      celular: [null, [Validators.required]],
      endereco: this.formBuilder.group({
        cep: [null],
        numero: [null],
        complemento: [null],
        logradouro: [null],
        bairro: [null],
        cidade: [null],
        estado: [null],
      }),
    });
  }

  salvar() {

    const valueSubmit = Object.assign({}, this.vendedorForm.value);

    this.subscription = this.vendedorService.criar(valueSubmit)
    .subscribe( response => {
      this.router.navigate([`/vendedor`]);
      this.toasty.success(`Vendedor "${response.nome}" cadastrado com sucesso`);
    }, err => {
      this.toasty.error('Erro ao cadastrar vendedor');
    });
  }

  consultaCEP() {
    const cep = this.vendedorForm.get('endereco.cep').value;

    if (cep != null && cep !== '') {
      this.cepService.consultaCEP(cep)
      .pipe(map(dados => dados))
      .subscribe(dados => {
        this.populaDadosForm(dados);
      }, err => {
        this.toasty.error('Erro ao consultar CEP');
      });
    }
  }

  populaDadosForm(dados) {

    this.vendedorForm.patchValue({
      endereco: {
        cep: dados.cep,
        logradouro: dados.logradouro,
        bairro: dados.bairro,
        cidade: dados.localidade,
        estado: dados.uf
        }
    });
  }

  setNav(visibilidade: boolean) {
    this.nav = visibilidade;
  }

  resetar(vendedorForm: FormGroup) {
    vendedorForm.reset();
  }

  voltarLista() {
    this.router.navigate([`/vendedor`]);
  }

}
