import { EnderecoForm } from './EnderecoForm';

export interface VendedorForm {
  nome: string;
  email: string;
  cpf: string;
  telefone: string;
  celular: string;
  endereco: EnderecoForm;
  ativo: boolean;
}
