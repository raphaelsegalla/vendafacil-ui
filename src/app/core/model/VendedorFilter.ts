export class VendedorFilter {
  nome: string;
  email: string;
  telefone: string;
  logradouro: string;
  pagina = 0;
  itensPorPagina = 8;
}
