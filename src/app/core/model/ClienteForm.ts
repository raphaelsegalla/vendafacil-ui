import { EnderecoForm } from './EnderecoForm';

export interface ClienteForm {
  nome: string;
  email: string;
  cpf: string;
  telefone: string;
  celular: string;
  endereco: EnderecoForm;
  situacao: boolean;
}

