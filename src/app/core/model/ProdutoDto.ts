export interface ProdutoDto {
  codigo: number;
  nome: string;
  tipo: string;
  comprimento: number;
  largura: number;
  espessura: number;
  numero: number;
  peso: number;
  dataCompra: Date;
  valorCompra: number;
  valorVenda: number;
  vendido: boolean;
}
