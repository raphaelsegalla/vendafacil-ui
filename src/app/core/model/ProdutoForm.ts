export interface ProdutoForm {
  nome: string;
  tipo: string;
  comprimento: number;
  largura: number;
  espessura: number;
  numero: number;
  peso: number;
  dataCompra: Date;
  valorCompra: number;
  valorVenda: number;
  observacao: string;
  vendido: boolean;
}
