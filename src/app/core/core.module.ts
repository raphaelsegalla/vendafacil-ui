import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { NavbarComponent } from './navbar/navbar.component';
import { RouterModule } from '@angular/router';
import { ErrorHandlerService } from './error-handler.service';
import { ToastyModule } from 'ng2-toasty';

@NgModule({
  declarations: [
    NavbarComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,
    ToastyModule.forRoot()
  ],
  exports: [
    NavbarComponent,
    ToastyModule
  ],
  providers: [
    ErrorHandlerService
  ]
})
export class CoreModule { }
