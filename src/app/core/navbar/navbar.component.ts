import { Component, OnInit } from '@angular/core';
import { NavbarService } from './navbar.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  nav = false;
  showSubmenu = false;
  showSubmenu1 = false;
  showSubmenu2 = false;
  showSubmenu3 = false;
  showSubmenu4 = false;

  constructor(private navbarService: NavbarService) { }

  ngOnInit() {
  }

  mudarNav() {
    this.nav = !this.nav;
    this.navbarService.setNav(this.nav);
  }

  mostrarMenu(valor: boolean, numero: number) {
    if (valor) {
      switch (valor) {
        case numero === 1: {
          this.showSubmenu = true;
          this.showSubmenu1 = false;
          this.showSubmenu2 = false;
          this.showSubmenu3 = false;
          this.showSubmenu4 = false;
          break;
        }
        case numero === 2: {
          this.showSubmenu = false;
          this.showSubmenu1 = true;
          this.showSubmenu2 = false;
          this.showSubmenu3 = false;
          this.showSubmenu4 = false;
          break;
        }
        case numero === 3: {
          this.showSubmenu = false;
          this.showSubmenu1 = false;
          this.showSubmenu2 = true;
          this.showSubmenu3 = false;
          this.showSubmenu4 = false;
          break;
        }
        case numero === 4: {
          this.showSubmenu = false;
          this.showSubmenu1 = false;
          this.showSubmenu2 = false;
          this.showSubmenu3 = true;
          this.showSubmenu4 = false;
          break;
        }
        case numero === 5: {
          this.showSubmenu = false;
          this.showSubmenu1 = false;
          this.showSubmenu2 = false;
          this.showSubmenu3 = false;
          this.showSubmenu4 = true;
          break;
        }
        default: {
           break;
        }
      }
    } else {
      this.showSubmenu = false;
      this.showSubmenu1 = false;
      this.showSubmenu2 = false;
      this.showSubmenu3 = false;
      this.showSubmenu4 = false;
    }
  }

}
