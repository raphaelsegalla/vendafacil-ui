import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NavbarService {

  eventContainer = new EventEmitter<any>();
  private nav = false;

  constructor() { }

  getNav() {
    return this.nav;
  }

  setNav(val: boolean) {
    this.nav = val;
    this.eventContainer.emit(val);
  }
}
