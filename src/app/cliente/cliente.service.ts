import { Injectable, EventEmitter } from '@angular/core';
import { HttpHeaders, HttpParams, HttpClient } from '@angular/common/http';
import { ClienteFilter } from '../core/model/ClienteFilter';
import { Observable } from 'rxjs';
import { ClienteForm } from '../core/model/ClienteForm';

@Injectable()
export class ClienteService {

  clienteUrl = 'http://localhost:8080/clientes';

  constructor(private http: HttpClient) { }

   httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      Authorization: 'Basic YWRtaW5AZ21haWwuY29tOmFkbWlu'
    })
  };

  listarClientes(filtro: ClienteFilter): Observable<any> {

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: 'Basic YWRtaW5AZ21haWwuY29tOmFkbWlu'
      }),
      params: new HttpParams()
      .set('nome', filtro.nome === undefined ? '' : filtro.nome)
      .set('email', filtro.email === undefined ? '' : filtro.email)
      .set('telefone', filtro.telefone === undefined ? '' : filtro.telefone)
      .set('logradouro', filtro.logradouro === undefined ? '' : filtro.logradouro)
      .set('page', filtro.pagina.toString())
      .set('size', filtro.itensPorPagina.toString())

    };
    return this.http.get(`${this.clienteUrl}`, httpOptions );
  }

  criar(cliente: ClienteForm): Observable<ClienteForm> {
    cliente.situacao = true;
    return this.http.post<ClienteForm>(`${this.clienteUrl}`, cliente, this.httpOptions );
  }

  excluir(codigo: number) {
    return this.http.delete(`${this.clienteUrl}/${codigo}`, this.httpOptions);
  }
}
