import { Component, OnInit, Input } from '@angular/core';
import { ClienteFilter } from 'src/app/core/model/ClienteFilter';
import { PesquisaClienteComponent } from '../pesquisa-cliente/pesquisa-cliente.component';
import { ClienteService } from '../cliente.service';
import { LazyLoadEvent, ConfirmationService } from 'primeng/primeng';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { ToastyService } from 'ng2-toasty';

@Component({
  selector: 'app-lista-cliente',
  templateUrl: './lista-cliente.component.html',
  styleUrls: ['./lista-cliente.component.css'],
  animations: [
    trigger('rowExpansionTrigger', [
        state('void', style({
            transform: 'translateX(-10%)',
            opacity: 0
        })),
        state('active', style({
            transform: 'translateX(0)',
            opacity: 1
        })),
        transition('* <=> *', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)'))
    ])
 ]
})
export class ListaClienteComponent implements OnInit {

  @Input() clientes = [];
  @Input() totalRegistros: number;
  @Input() filtro = new ClienteFilter();

  pagina: number;

  constructor(
    private pesquisaCliente: PesquisaClienteComponent,
    private clienteService: ClienteService,
    private toasty: ToastyService,
    private confirmationService: ConfirmationService
  ) { }

  ngOnInit() {
  }

  aoMudarPagina(event: LazyLoadEvent ) {
    const pagina = event.first / event.rows;
    this.pagina = pagina;
    this.pesquisaCliente.pesquisar(pagina);
  }

  excluir(cliente: any) {
    this.clienteService.excluir(cliente.codigo)
    .subscribe( () => {
      this.pesquisaCliente.pesquisar(this.pagina);
      this.toasty.success(`Cliente "${cliente.nome}" excluido com sucesso`);
    }, err => {
      this.toasty.error('Erro ao excluir produto');
    });
  }

  confirmarExclusao(cliente: any) {
    this.confirmationService.confirm({
      message: `Tem certeza que deseja excluir o(a) cliente "${cliente.nome}"`,
      accept: () => {
        this.excluir(cliente);
      }
    });
  }

}
