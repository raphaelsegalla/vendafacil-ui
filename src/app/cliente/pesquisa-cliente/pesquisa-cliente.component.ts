import { Component, OnInit, HostBinding } from '@angular/core';
import { ClienteFilter } from 'src/app/core/model/ClienteFilter';
import { ClienteService } from '../cliente.service';
import { NavbarService } from 'src/app/core/navbar/navbar.service';
import { Router } from '@angular/router';
import { ToastyService } from 'ng2-toasty';

@Component({
  selector: 'app-pesquisa-cliente',
  templateUrl: './pesquisa-cliente.component.html',
  styleUrls: ['./pesquisa-cliente.component.css']
})
export class PesquisaClienteComponent implements OnInit {

  filtro = new ClienteFilter();
  clientes = [];
  totalRegistros = 0;
  nav = false;

  constructor(
    private clienteService: ClienteService,
    private navbarService: NavbarService,
    private router: Router,
    private toasty: ToastyService
  ) { }

  ngOnInit() {
    this.navbarService.eventContainer.subscribe((event: boolean) => this.setNav(event));
    this.pesquisar();
  }

  pesquisar(pagina = 0) {

    this.filtro.pagina = pagina;

    this.clienteService.listarClientes(this.filtro)
      .subscribe(response => {
        this.clientes = response.content;
        this.totalRegistros = response.totalElements;
      }, err => {
        this.toasty.error('Erro ao listar clientes');
      });
  }

  setNav(visibilidade: boolean) {
    this.nav = visibilidade;
  }

  novoCliente() {
    this.router.navigate([`/cadastraCliente`]);
  }

}
