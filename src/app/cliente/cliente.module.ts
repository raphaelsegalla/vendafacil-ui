import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { CurrencyMaskModule } from 'ng2-currency-mask';

import { PesquisaClienteComponent } from './pesquisa-cliente/pesquisa-cliente.component';
import { ListaClienteComponent } from './lista-cliente/lista-cliente.component';
import { CadastraClienteComponent } from './cadastra-cliente/cadastra-cliente.component';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    PesquisaClienteComponent,
    ListaClienteComponent,
    CadastraClienteComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    SharedModule,
    CurrencyMaskModule,
    RouterModule
  ],
  exports: [
    PesquisaClienteComponent,
    CadastraClienteComponent
  ]
})
export class ClienteModule { }
