import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ClienteService } from '../cliente.service';
import { Subscription } from 'rxjs';
import { ConsultaCepService } from 'src/app/shared/services/consulta-cep.service';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { NavbarService } from 'src/app/core/navbar/navbar.service';
import { ToastyService } from 'ng2-toasty';

@Component({
  selector: 'app-cadastra-cliente',
  templateUrl: './cadastra-cliente.component.html',
  styleUrls: ['./cadastra-cliente.component.css']
})
export class CadastraClienteComponent implements OnInit {

  subscription: Subscription;
  clienteForm: FormGroup;
  nav = false;

  constructor(
    private clienteService: ClienteService,
    private cepService: ConsultaCepService,
    private formBuilder: FormBuilder,
    private router: Router,
    private navbarService: NavbarService,
    private toasty: ToastyService
  ) { }

  ngOnInit() {
    this.navbarService.eventContainer.subscribe(
      ( event: boolean) => this.setNav(event)
    );

    this.clienteForm = this.formBuilder.group({
      nome: [null, [Validators.required, Validators.minLength(5), Validators.maxLength(50)]],
      email: [null, [Validators.required, Validators.email]],
      cpf: [null],
      telefone: [null, [Validators.required]],
      celular: [null, [Validators.required]],
      endereco: this.formBuilder.group({
        cep: [null],
        numero: [null],
        complemento: [null],
        logradouro: [null],
        bairro: [null],
        cidade: [null],
        estado: [null],
      }),
    });
  }

  salvar() {

    const valueSubmit = Object.assign({}, this.clienteForm.value);

    this.subscription = this.clienteService.criar(valueSubmit)
    .subscribe( response => {
      this.router.navigate([`/cliente`]);
      this.toasty.success(`Cliente "${response.nome}" cadastrado com sucesso`);
    }, err => {
      this.toasty.error('Erro ao cadastrar cliente');
    });
  }

  consultaCEP() {
    const cep = this.clienteForm.get('endereco.cep').value;

    if (cep != null && cep !== '') {
      this.cepService.consultaCEP(cep)
      .pipe(map(dados => dados))
      .subscribe(dados => {
        this.populaDadosForm(dados);
      }, err => {
        this.toasty.error('Erro ao consultar CEP');
      });
    }
  }

  populaDadosForm(dados) {

    this.clienteForm.patchValue({
      endereco: {
        cep: dados.cep,
        logradouro: dados.logradouro,
        bairro: dados.bairro,
        cidade: dados.localidade,
        estado: dados.uf
        }
    });
  }

  setNav(visibilidade: boolean) {
    this.nav = visibilidade;
  }

  aplicaCssErro(nome) {
    return true;
  }

  resetar(clienteForm: FormGroup) {
    clienteForm.reset();
  }

  voltarLista() {
    this.router.navigate([`/cliente`]);
  }

}
