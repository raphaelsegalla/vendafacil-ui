import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ProdutoService } from '../produto.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { NavbarService } from 'src/app/core/navbar/navbar.service';
import { ToastyService } from 'ng2-toasty';

@Component({
  selector: 'app-cadastra-produto',
  templateUrl: './cadastra-produto.component.html',
  styleUrls: ['./cadastra-produto.component.css']
})
export class CadastraProdutoComponent implements OnInit {

  subscription: Subscription;
  nav = false;
  ptbr: any;

  tipos = [
    { label: 'Corrente', value: 'CORRENTE'},
    { label: 'Pulseira', value: 'PULSEIRA'},
    { label: 'Anel', value: 'ANEL'},
    { label: 'Pingente', value: 'PINGENTE'},
    { label: 'Brinco', value: 'BRINCO'},
    { label: 'Bracelete', value: 'BRACELETE'},
    { label: 'Tornozeleira', value: 'TORNOZELEIRA'}
  ];

  constructor(
    private produtoService: ProdutoService,
    private navbarService: NavbarService,
    private router: Router,
    private toasty: ToastyService
  ) { }

  ngOnInit() {
    this.navbarService.eventContainer.subscribe(
      (event: boolean) => this.setNav(event)
    );
    this.ptbr = {
      firstDayOfWeek: 0,
      dayNames: ['Domingo', 'Segunda-feira', 'Terça-feira', 'Quarta-feira', 'Quinta-feira', 'Sexta-feira', 'Sábado'],
      dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
      dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
      monthNames: [ 'Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho',
                    'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'
                  ],
      monthNamesShort: [ 'Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez' ],
      today: 'Hoje',
      clear: 'Limpar',
      dateFormat: 'dd/mm/yy',
      weekHeader: 'Sem'
    };
  }

  salvar(form: NgForm) {
    this.subscription = this.produtoService.criar(form.value)
    .subscribe( response => {
      this.router.navigate([`/produto`]);
      this.toasty.success(`Produto "${response.nome}" cadastrado com sucesso`);
    }, err => {
      this.toasty.error('Erro ao cadastrar produto');
    });
  }

  setNav(visibilidade: boolean) {
    this.nav = visibilidade;
  }

  resetar(form: NgForm) {
    form.reset();
  }

}
