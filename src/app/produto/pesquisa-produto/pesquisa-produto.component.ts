import { Component, OnInit } from '@angular/core';
import { ProdutoFilter } from 'src/app/core/model/ProdutoFilter';
import { ProdutoService } from '../produto.service';
import { NavbarService } from 'src/app/core/navbar/navbar.service';
import { Router } from '@angular/router';
import { ToastyService } from 'ng2-toasty';

@Component({
  selector: 'app-pesquisa-produto',
  templateUrl: './pesquisa-produto.component.html',
  styleUrls: ['./pesquisa-produto.component.css']
})
export class PesquisaProdutoComponent implements OnInit {

  filtro = new ProdutoFilter();
  produtos = [];
  totalRegistros = 0;
  nav = false;

  constructor(
    private produtoService: ProdutoService,
    private navbarService: NavbarService,
    private router: Router,
    private toasty: ToastyService
  ) { }

  tipos = [
    { label: 'Corrente', value: 'CORRENTE'},
    { label: 'Pulseira', value: 'PULSEIRA'},
    { label: 'Anel', value: 'ANEL'},
    { label: 'Pingente', value: 'PINGENTE'},
    { label: 'Brinco', value: 'BRINCO'},
    { label: 'Bracelete', value: 'BRACELETE'},
    { label: 'Tornozeleira', value: 'TORNOZELEIRA'}
  ];

  ngOnInit() {
    this.pesquisar();

    this.navbarService.eventContainer.subscribe(
      (event: boolean) => this.setNav(event)
    );
  }

  pesquisar(pagina = 0) {

    this.filtro.pagina = pagina;

    if (this.filtro.tipo == null) {
      this.filtro.tipo = '';
    }

    this.produtoService.listarProdutos(this.filtro)
      .subscribe(response => {
        this.produtos = response.content;
        this.totalRegistros = response.totalElements;
      }, err => {
        this.toasty.error('Erro ao listar produtos');
      });
  }

  setNav(visibilidade: boolean) {
    this.nav = visibilidade;
  }

  novoProduto() {
    this.router.navigate([`/cadastraProduto`]);
  }

}
