import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';

import { CurrencyMaskModule } from 'ng2-currency-mask';

import { ListaProdutosComponent } from './lista-produtos/lista-produtos.component';
import { CadastraProdutoComponent } from './cadastra-produto/cadastra-produto.component';
import { PesquisaProdutoComponent } from './pesquisa-produto/pesquisa-produto.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [
    ListaProdutosComponent,
    CadastraProdutoComponent,
    PesquisaProdutoComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    BrowserAnimationsModule,
    SharedModule,
    CurrencyMaskModule
  ],
  exports: [
    PesquisaProdutoComponent,
    CadastraProdutoComponent
  ]
})
export class ProdutoModule { }
