import { Component, OnInit, Input } from '@angular/core';
import { ProdutoService } from '../produto.service';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { ProdutoFilter } from 'src/app/core/model/ProdutoFilter';
import { PesquisaProdutoComponent } from '../pesquisa-produto/pesquisa-produto.component';
import { LazyLoadEvent, ConfirmationService } from 'primeng/primeng';
import { ToastyService } from 'ng2-toasty';

@Component({
  selector: 'app-lista-produtos',
  templateUrl: './lista-produtos.component.html',
  styleUrls: ['./lista-produtos.component.css'],
  animations: [
    trigger('rowExpansionTrigger', [
        state('void', style({
            transform: 'translateX(-10%)',
            opacity: 0
        })),
        state('active', style({
            transform: 'translateX(0)',
            opacity: 1
        })),
        transition('* <=> *', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)'))
    ])
]
})
export class ListaProdutosComponent implements OnInit {

  @Input() totalRegistros: number;
  @Input() produtos = [];
  @Input() filtro = new ProdutoFilter();

  pagina: number;

  constructor(
    private pesquisaProduto: PesquisaProdutoComponent,
    private produtoService: ProdutoService,
    private toasty: ToastyService,
    private confirmationService: ConfirmationService
  ) { }

  ngOnInit() {
  }

  aoMudarPagina( event: LazyLoadEvent ) {
    const pagina = event.first / event.rows;
    this.pagina = pagina;
    this.pesquisaProduto.pesquisar(pagina);
  }

  excluir(produto: any) {
    this.produtoService.excluir(produto.codigo)
    .subscribe( () => {
      this.pesquisaProduto.pesquisar(this.pagina);
      this.toasty.success(`Produto "${produto.nome}" excluido com sucesso`);
    }, err => {
      this.toasty.error('Erro ao excluir produto');
    });
  }

  confirmarExclusao(produto: any) {
    this.confirmationService.confirm({
      message: `Tem certeza que deseja excluir o produto "${produto.nome}"`,
      accept: () => {
        this.excluir(produto);
      }
    });
  }
}
