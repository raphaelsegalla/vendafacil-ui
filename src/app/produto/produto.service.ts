import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { ProdutoForm } from '../core/model/ProdutoForm';
import { ProdutoFilter } from '../core/model/ProdutoFilter';
import { ProdutoDto } from '../core/model/ProdutoDto';

@Injectable()
export class ProdutoService {

  produtosUrl = 'http://localhost:8080/produtos';

  constructor(private http: HttpClient) { }

   httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      Authorization: 'Basic YWRtaW5AZ21haWwuY29tOmFkbWlu'
    })
  };

  listarProdutos(filtro: ProdutoFilter): Observable<any> {

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: 'Basic YWRtaW5AZ21haWwuY29tOmFkbWlu'
      }),
      params: new HttpParams()
      .set('nome', filtro.nome === undefined ? '' : filtro.nome)
      .set('tipo', filtro.tipo === undefined ? '' : filtro.tipo)
      .set('page', filtro.pagina.toString())
      .set('size', filtro.itensPorPagina.toString())

    };
    return this.http.get(`${this.produtosUrl}`, httpOptions );
  }

  criar(produto: ProdutoForm): Observable<ProdutoForm> {
    produto.vendido = true;
    return this.http.post<ProdutoForm>(`${this.produtosUrl}`, produto, this.httpOptions );
  }

  excluir(codigo: number) {
    return this.http.delete(`${this.produtosUrl}/${codigo}`, this.httpOptions);
  }

}
